from InquirerPy import inquirer
from InquirerPy.validator import NumberValidator
from prompt_toolkit.validation import ValidationError, Validator
from ipaddress import IPv4Interface
from pathlib import Path
from typing import List
from datetime import datetime
from wgconf.utils import (
    exit_if_no_wg_tools,
    gen_key_pair,
    gen_client_keys,
    create_confs,
    is_valid_network,
    has_enough_addresses,
)


def prompts() -> List[str]:
    num_clients = inquirer.text(
        message="Enter number of devices:",
        validate=NumberValidator(),
        filter=lambda result: int(result),
    ).execute()

    class NetworkValidator(Validator):
        def validate(self, document):
            if not is_valid_network(document.text):
                raise ValidationError(
                    message="Invalid CIDR",
                    cursor_position=document.cursor_position,
                )
            elif not has_enough_addresses(document.text, num_clients):
                raise ValidationError(
                    message="Not enough addresses in network:",
                    cursor_position=document.cursor_position,
                )

    network = inquirer.text(
        message="Enter a network in CIDR Notation",
        validate=NetworkValidator(),
        filter=lambda result: IPv4Interface(result),
        default="10.0.0.0/24",
    ).execute()

    endpoint = inquirer.text(
        message="Enter an endpoint for your Wireguard server (e.g. an IP address or hostname):",
    ).execute()

    port = inquirer.text(
        message="Enter a port for your endpoint. Press Enter for default:",
        validate=NumberValidator(),
        filter=lambda result: int(result),
        default="51820",
    ).execute()

    allowed_ips = inquirer.text(
        message="Which IP addresses are allowed to communicate with your client?",
        default=str(network),
    ).execute()

    config_path = inquirer.filepath(
        message="Save path:",
        default=str(Path(".").resolve() / f"configs_{datetime.today()}"),
        filter=lambda result: Path(result).expanduser(),
    ).execute()

    return [num_clients, network, endpoint, port, allowed_ips, config_path]


def main():
    exit_if_no_wg_tools()
    num_clients, network, endpoint, port, allowed_ips, config_path = prompts()

    server_key_pair = gen_key_pair()
    client_keys_dict = gen_client_keys(num_clients)

    server_conf, client_confs = create_confs(
        server_key_pair["private_key"],
        server_key_pair["public_key"],
        client_keys_dict,
        network,
        endpoint,
        port,
        allowed_ips,
        config_path,
    )

    server_conf.write_conf_to_file("01_server.conf", server_conf.make_conf())
    if num_clients <= 20:
        server_conf.generate_qr_code("01_server")

    for i, conf in enumerate(client_confs, start=1):
        conf.write_conf_to_file(f"{i + 1:02d}_client_{i}.conf", conf.make_conf())
        conf.generate_qr_code(f"{i+1:02d}_client_{i}")

    print(
        f"\nWireguard Server: {conf.endpoint}:{server_conf.port}\n"
        f"{i} clients\n"
        f"Configuration files and QR codes stored in {config_path.resolve()}"
    )


if __name__ == "__main__":
    main()
