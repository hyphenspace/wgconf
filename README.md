# wgconf - Another Wireguard configuration Generator

CLI prompt to get a minimally working set of Wireguard configuration files.

## Installation

`pip install wgconf`

## Basic Usage

[Wireguard-tools](https://www.wireguard.com/install/) should be installed before using this tool.

After that, just go through the prompts and you'll have a set of configuration files and corresponding QR codes.

## Post Install
You'll likely need to do some port forwarding to your Wireguard server*. After that a `wg-quick up 01_server.conf` (or whatever you name the file) should bring up the wg interface and it should be accessible by your clients.

*Strictly speaking, Wireguard doesn't really have a concept of servers vs clients as everything is just a peer. 
